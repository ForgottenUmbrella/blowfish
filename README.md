# Blowfish
Blowfish encryption algorithm C++ implementation

## CMake Usage
1. Download this repo somewhere (perhaps as a git submodule).
1. In your `CMakeLists.txt`:
```cmake
add_subdirectory(<blowfish_repo_location>)
target_include_directories(<target> <INTERFACE|PUBLIC|PRIVATE> <blowfish_repo_location>)
target_link_libraries(<target> <INTERFACE|PUBLIC|PRIVATE> Blowfish)
```
Substitute `<target>` with the name of your target and
`<blowfish_repo_location>` with the path in which you downloaded this repo, and
select one of [`<INTERFACE|PUBLIC|PRIVATE>`][interface public private] depending
on whether or not `Blowfish` is an implementation detail.

## API
The `Blowfish` class is defined in the `blowfish.h` header.

It is constructed with a key of type `std::vector<char>`.

To encrypt text, call the `Encrypt` method, which takes plaintext of type
`std::vector<char>` and returns cyphertext of type `std::vector<char>`.

To decrypt text, call the `Decrypt` method, which takes cyphertext of type
`std::vector<char>` and returns plaintext of type `std::vector<char>`.

Note that to construct a `std::vector<char>` from a `std::string`:
```cpp
auto my_vector = std::vector<char>{my_string.begin(), my_string.end()};
```
And to do the opposite:
```cpp
auto my_string = std::string{my_vector.begin(), my_vector.end()};
```

## Notes
 * Tested on Linux and OSX
 * ECB mode only
 * The key length must be a multiple of 8 bits
 * PKCS #5 Padding

## License
[![CC0 image][]][CC0 license]

[interface public private]: https://cmake.org/pipermail/cmake/2016-May/063400.html
[CC0 image]: http://i.creativecommons.org/p/zero/1.0/88x31.png
[CC0 license]: http://creativecommons.org/publicdomain/zero/1.0/
